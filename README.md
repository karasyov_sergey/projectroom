# Полезные источники информации:

1. [Перевод туториалов по libGdx часть 2](https://habr.com/ru/post/143405/)

2. [Перевод туториалов по libGdx часть 3](https://habr.com/ru/post/143479/)

3. [libgdx туториал по созданию игры часть 1](https://habr.com/ru/post/224175/)

4. [libgdx туториал по созданию игры часть 2](https://habr.com/ru/post/224223/)

5. [libgdx практические вопросы и ответы](https://habr.com/ru/post/338398/)

6. [libgdx создание клона Flappy Bird](https://habr.com/ru/post/243471/)

7. [libgdx простой текстовый ввод](http://www.libgdx.ru/2013/10/simple-text-input.html)

8. [документация eng](https://libgdx.badlogicgames.com/ci/nightlies/docs/api/)

9. [примеры unit тестов](https://github.com/libgdx/libgdx/tree/master/tests/gdx-tests/src/com/badlogic/gdx/tests)

Каждую задачу реализовывать в отдельной ветке.

Также пополняйте список полезных источников информации новыми ссылками!!!