package ru.pitchblackteam.game.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

/**
 * Класс для представления
 * объекта "стрелка"
 *
 * @author Анастасия Лисова, 17ИТ17
 */
public class Arrow {
    private Texture texture;
    private Rectangle rectangle;
    private Vector3 touchPos;

    public Arrow(Texture texture, int x, int y) {
        this.texture = texture;
        this.rectangle = new Rectangle();
        this.touchPos = new Vector3();
        rectangle.x = x;
        rectangle.y = y;
        rectangle.height = texture.getHeight();
        rectangle.width = texture.getWidth();
    }

    public boolean isTouched(OrthographicCamera camera) {
        if (Gdx.input.justTouched()) {
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);
            boolean overlapsByX = touchPos.x >= rectangle.x - rectangle.width &&
                    touchPos.x <= rectangle.x + rectangle.width * 2;
            boolean overlapsByY = touchPos.y >= rectangle.y &&
                    touchPos.y <= rectangle.y + rectangle.height;
            return overlapsByX && overlapsByY;
        }
        return false;
    }

    public void render(SpriteBatch spriteBatch) {
        spriteBatch.draw(texture, rectangle.x, rectangle.y);
    }

    public void dispose() {
        texture.dispose();

    }
}
