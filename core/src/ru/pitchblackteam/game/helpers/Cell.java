package ru.pitchblackteam.game.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.objects.GameItem;

public class Cell {

    private Rectangle rectangle;
    private Texture cellTexture;
    private GameItem cellContent;
    private static int nextPosition = 0;
    private Vector3 touchPos;
    private OrthographicCamera camera;
    private boolean overlapsByX;
    private boolean overlapsByY;

    Cell() {
        setCellTexture();
        rectangle = new Rectangle();

        rectangle.width = cellTexture.getWidth();
        rectangle.height = cellTexture.getHeight();
        nextPosition += rectangle.getHeight();

        rectangle.x = Room.WIDTH - rectangle.width;
        rectangle.y = Room.HEIGHT - nextPosition - 5;

        touchPos = new Vector3();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, Room.WIDTH, Room.HEIGHT);

        overlapsByX = false;
        overlapsByY = false;

    }

    void setCellTexture() {
        cellTexture = TextureLoader.cell;
    }

    void setHighlightedCellTexture() {
        cellTexture = TextureLoader.highlightedCell;
    }

    public void draw(SpriteBatch batch) {
        batch.draw(cellTexture, rectangle.x, rectangle.y);
        if (!isEmpty()) {
            cellContent.render(batch);
        }
    }

    public static void clear() {
        nextPosition = 0;
    }

    void addItem(GameItem cellContent) {
        this.cellContent = cellContent;
    }

    void deleteItem() {
        cellContent = null;
    }

    public float getX() {
        return rectangle.x;

    }

    public float getY() {
        return rectangle.y;
    }

    public float getWidth() {
        return rectangle.width;
    }

    public float getHeight() {
        return rectangle.height;
    }

    public GameItem getCellContent() {
        return cellContent;
    }

    public boolean justTouched() {
        if (Gdx.input.justTouched()) {
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);
            overlapsByX = touchPos.x >= rectangle.x &&
                    touchPos.x <= rectangle.x + rectangle.width;
            overlapsByY = touchPos.y >= rectangle.y &&
                    touchPos.y <= rectangle.y + rectangle.height;
            return overlapsByX && overlapsByY;
        }
        return false;
    }

    public boolean isEmpty() {
        return cellContent == null;
    }


}
