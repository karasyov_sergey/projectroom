package ru.pitchblackteam.game.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;

public class Data {
    public static Array<String> bookOneData;

    public static void initialData() {
        bookOneData = new Array<>();
        bookOneData.add(Gdx.files.internal("data/note1.txt").readString());
        bookOneData.add(Gdx.files.internal("data/note2.txt").readString());
        bookOneData.add(Gdx.files.internal("data/note3.txt").readString());
        bookOneData.add(Gdx.files.internal("data/note4.txt").readString());
        bookOneData.add(Gdx.files.internal("data/note5.txt").readString());
    }
}
