package ru.pitchblackteam.game.helpers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

import ru.pitchblackteam.game.objects.GameItem;

public class Inventory {

    private Array<Cell> cells;
    private int index;


    public Inventory() {
        cells = new Array<>();
        index = -1;
        createCells();
    }

    private void createCells() {
        for (int i = 0; i < 5; i++) {
            cells.add(new Cell());
        }
    }

    public void addItem(GameItem item) {
        for (int i = 0; i < cells.size; i++) {
            if (cells.get(i).isEmpty()) {
                item.toMinSize();
                item.setX((int) cells.get(i).getX());
                item.setY((int) cells.get(i).getY());
                cells.get(i).addItem(item);
                break;
            }
        }
    }


    public void render(SpriteBatch batch) {
        for (Cell cell : cells) {
            cell.draw(batch);
        }
    }

    public void fixateTouch() {
        for (int i = 0; i < cells.size; i++) {
            if (cells.get(i).justTouched()) {
                if (i != index) {
                    cells.get(i).setHighlightedCellTexture();
                    if (index != -1) cells.get(index).setCellTexture();
                    index = i;
                } else {
                    cells.get(i).setCellTexture();
                    index = -1;
                }
            }
        }
    }

    public Cell getTouchedCell() {
        return cells.get(index);
    }

    public void deleteItem() {
        cells.get(index).deleteItem();
    }

    public boolean haveGot(GameItem gameItem) {
        for (Cell cell : cells) {
            if (cell.getCellContent() != null && cell.getCellContent().equals(gameItem)) {
                return true;
            }
        }
        return false;
    }

    public boolean justTouched() {
        for (int i = 0; i < cells.size; i++) {
            if (cells.get(i).justTouched()) {
                return true;
            }
        }

        return false;
    }


}
