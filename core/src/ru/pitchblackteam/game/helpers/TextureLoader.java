package ru.pitchblackteam.game.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

/**
 * Класс, хранящий в себе
 * текстуры всех объектов
 *
 * @author Лисова Анастасия, 17ИТ17
 */
public class TextureLoader {
    public static Texture bigMirror;
    public static Texture smallMirror;
    public static Texture safe;
    public static Texture bigBoxOne;
    public static Texture bigBoxTwo;
    public static Texture bigBoxThree;
    public static Texture bigBoxFour;
    public static Texture smallBoxOne;
    public static Texture smallBoxTwo;
    public static Texture smallBoxThree;
    public static Texture smallBoxFour;
    public static Texture bigPicture;
    public static Texture smallPicture;
    public static Texture door;
    public static Texture safeButtonBig;
    public static Texture safeButtonSmall;
    public static Texture smallSafe;
    public static Texture buttonDown;
    public static Texture book;
    public static Texture bookPage;
    public static Texture buttonLeft;
    public static Texture buttonRight;
    public static Texture boxBackground;
    public static Texture mainBackground;
    public static Texture cell;
    public static Texture bigNail;
    public static Texture smallNail;
    public static Texture bigRope;
    public static Texture smallRope;
    public static Texture smallSheet;
    public static Texture bigSheet;
    public static Texture itemBox;
    public static Texture abyss;
    public static Texture light;
    public static Texture trumpet;
    public static Texture inventoryTrumpet;
    public static Texture hammer;
    public static Texture inventoryHammer;
    public static Texture lighter;
    public static Texture inventoryLighter;
    public static Texture smallContainer;
    public static Texture bigContainer;
    public static Texture buttonSecret;
    public static Texture cap;
    public static Texture shadow;
    public static Texture highlightedCell;


    public static void load() {
        bigMirror = new Texture(Gdx.files.internal("bigtextures/big-mirror.png"));
        smallMirror = new Texture(Gdx.files.internal("smalltextures/small-mirror.png"));
        safe = new Texture(Gdx.files.internal("bigtextures/big-safe.png"));
        bigBoxOne = new Texture(Gdx.files.internal("bigtextures/big-bedside1.png"));
        bigBoxTwo = new Texture(Gdx.files.internal("bigtextures/big-bedside2.png"));
        bigBoxThree = new Texture(Gdx.files.internal("bigtextures/big-bedside3.png"));
        bigBoxFour = new Texture(Gdx.files.internal("bigtextures/big-bedside4.png"));
        smallBoxOne = new Texture(Gdx.files.internal("smalltextures/small-bedside1.png"));
        smallBoxTwo = new Texture(Gdx.files.internal("smalltextures/small-bedside2.png"));
        smallBoxThree = new Texture(Gdx.files.internal("smalltextures/small-bedside3.png"));
        smallBoxFour = new Texture(Gdx.files.internal("smalltextures/small-bedside4.png"));
        bigPicture = new Texture(Gdx.files.internal("bigtextures/big-picture.png"));
        smallPicture = new Texture(Gdx.files.internal("smalltextures/small-picture.png"));
        door = new Texture(Gdx.files.internal("bigtextures/door.jpg"));
        safeButtonBig = new Texture(Gdx.files.internal("helpers/safeButton-big.png"));
        smallSafe = new Texture(Gdx.files.internal("smalltextures/small-safe.png"));
        safeButtonSmall = new Texture(Gdx.files.internal("helpers/safeButton-small.png"));
        buttonDown = new Texture(Gdx.files.internal("helpers/buttonDown.png"));
        book = new Texture(Gdx.files.internal("items/book.png"));
        bookPage = new Texture(Gdx.files.internal("items/book-page.png"));
        buttonLeft = new Texture(Gdx.files.internal("helpers/buttonLeft.png"));
        buttonRight = new Texture(Gdx.files.internal("helpers/buttonRight.png"));
        boxBackground = new Texture(Gdx.files.internal("helpers/boxBackground.png"));
        mainBackground = new Texture(Gdx.files.internal("helpers/mainBackground.jpg"));
        cell = new Texture(Gdx.files.internal("items/cell.png"));
        bigNail = new Texture(Gdx.files.internal("items/nail-big.png"));
        smallNail = new Texture(Gdx.files.internal("items/nail-small.png"));
        bigRope = new Texture(Gdx.files.internal("items/rope-big.png"));
        smallRope = new Texture(Gdx.files.internal("items/rope-small.png"));
        smallSheet = new Texture(Gdx.files.internal("items/sheet-small.png"));
        bigSheet = new Texture(Gdx.files.internal("items/sheet-big.png"));
        itemBox = new Texture(Gdx.files.internal("items/box2.png"));
        abyss = new Texture(Gdx.files.internal("items/abyss.png"));
        trumpet = new Texture(Gdx.files.internal("items/trumpet.png"));
        inventoryTrumpet = new Texture(Gdx.files.internal("items/trumpet-small.png"));
        hammer = new Texture(Gdx.files.internal("items/hammer.png"));
        inventoryHammer = new Texture(Gdx.files.internal("items/hammer-small.png"));
        lighter = new Texture(Gdx.files.internal("items/lighter.png"));
        inventoryLighter = new Texture(Gdx.files.internal("items/lighter-small.png"));
        smallContainer = new Texture(Gdx.files.internal("helpers/container-small.png"));
        bigContainer = new Texture(Gdx.files.internal("helpers/container-big.png"));
        buttonSecret = new Texture(Gdx.files.internal("helpers/buttonSecret.png"));
        light = new Texture(Gdx.files.internal("items/light.png"));
        cap = new Texture(Gdx.files.internal("helpers/cap.png"));
        shadow = new Texture(Gdx.files.internal("shadows/shadow.png"));
        highlightedCell = new Texture(Gdx.files.internal("helpers/HighlightedCell.png"));
    }

    public static void dispose() {
        bigMirror.dispose();
        smallMirror.dispose();
        safe.dispose();
        bigBoxOne.dispose();
        bigBoxTwo.dispose();
        bigBoxThree.dispose();
        bigBoxFour.dispose();
        smallBoxOne.dispose();
        smallBoxTwo.dispose();
        smallBoxThree.dispose();
        smallBoxFour.dispose();
        bigPicture.dispose();
        smallPicture.dispose();
        door.dispose();
        safeButtonBig.dispose();
        smallSafe.dispose();
        safeButtonSmall.dispose();
        buttonDown.dispose();
        book.dispose();
        bookPage.dispose();
        buttonLeft.dispose();
        buttonRight.dispose();
        boxBackground.dispose();
        mainBackground.dispose();
        cell.dispose();
        bigNail.dispose();
        smallNail.dispose();
        bigRope.dispose();
        smallRope.dispose();
        bigSheet.dispose();
        smallSheet.dispose();
        itemBox.dispose();
        abyss.dispose();
        trumpet.dispose();
        inventoryLighter.dispose();
        inventoryHammer.dispose();
        inventoryTrumpet.dispose();
        lighter.dispose();
        hammer.dispose();
        smallContainer.dispose();
        bigContainer.dispose();
        buttonSecret.dispose();
        cap.dispose();
        highlightedCell.dispose();
    }
}
