package ru.pitchblackteam.game.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

/**
 * Класс, хранящий в себе
 * звуки взаимодействия с
 * объектами
 *
 * @author Лисова Анастасия, 17ИТ17
 */
public class SoundLoader {
    public static Sound bedsideOpen;
    public static Sound bookOpen;
    public static Sound button;
    public static Sound hammer;
    public static Sound leaf;
    public static Sound mirror;
    public static Sound safeOpen;
    public static Music backgroundMusic;
    public static Sound growl;

    public static void load() {
        bedsideOpen = Gdx.audio.newSound(Gdx.files.internal("media/bedsideOpen.mp3"));
        bookOpen = Gdx.audio.newSound(Gdx.files.internal("media/bookOpen.mp3"));
        button = Gdx.audio.newSound(Gdx.files.internal("media/button.mp3"));
        hammer = Gdx.audio.newSound(Gdx.files.internal("media/hammer.mp3"));
        leaf = Gdx.audio.newSound(Gdx.files.internal("media/leaf.mp3"));
        mirror = Gdx.audio.newSound(Gdx.files.internal("media/mirror.mp3"));
        backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("media/backgroundMusic.mp3"));
        safeOpen = Gdx.audio.newSound(Gdx.files.internal("media/safeOpen.mp3"));
        growl = Gdx.audio.newSound(Gdx.files.internal("media/growl.mp3"));
    }

    public static void dispose() {
        bedsideOpen.dispose();
        bookOpen.dispose();
        button.dispose();
        hammer.dispose();
        leaf.dispose();
        mirror.dispose();
        safeOpen.dispose();
        growl.dispose();
    }
}
