package ru.pitchblackteam.game.gameworld;

import ru.pitchblackteam.game.animation.SmoothTransition;
import ru.pitchblackteam.game.enums.ItemType;
import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.helpers.TextureLoader;
import ru.pitchblackteam.game.objects.Book;
import ru.pitchblackteam.game.objects.GameItem;
import ru.pitchblackteam.game.objects.ItemBox;
import ru.pitchblackteam.game.objects.Sheet;

public class Items {
    public static Book book;
    public static GameItem light;
    public static GameItem trumpet;
    public static GameItem hammer;
    public static GameItem lighter;
    public static SmoothTransition smoothTransition;
    public static Sheet sheet;
    public static ItemBox itemBox;

    public static void createItems() {
        book = new Book((int) (Room.WIDTH / 2), 70);
        light = new GameItem(TextureLoader.light,
                TextureLoader.light, ItemType.LIGHT, 0, 0);
        trumpet = new GameItem(TextureLoader.inventoryTrumpet,
                TextureLoader.trumpet, ItemType.TRUMPET, 400, 200);
        hammer = new GameItem(TextureLoader.inventoryHammer,
                TextureLoader.hammer, ItemType.HAMMER, 0, 0);
        lighter = new GameItem(TextureLoader.inventoryLighter,
                TextureLoader.lighter, ItemType.LIGHTER, 0, 0);
        sheet = new Sheet("Don't believe him,\nread the numbers\nin descending order");
        itemBox = new ItemBox(100, 100);
        smoothTransition = new SmoothTransition();
    }
}
