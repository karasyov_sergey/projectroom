package ru.pitchblackteam.game.gameworld;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.helpers.SoundLoader;
import ru.pitchblackteam.game.helpers.TextureLoader;
import ru.pitchblackteam.game.objects.Bedside;
import ru.pitchblackteam.game.objects.Abyss;
import ru.pitchblackteam.game.objects.GameObject;
import ru.pitchblackteam.game.objects.Mirror;
import ru.pitchblackteam.game.objects.Picture;
import ru.pitchblackteam.game.objects.Safe;

public class MainSideWorld {
    private Room game;
    private Bedside bedside;
    private Abyss abyss;
    private Mirror mirror;
    private Picture picture;
    private Safe safe;
    private Array<GameObject> objects;
    private Sprite sprite;
    private boolean close;
    long time;

    public MainSideWorld(Room game) {
        this.game = game;
        time = 0;
        sprite = new Sprite(TextureLoader.mainBackground);
        sprite.setSize(710, 460);
        sprite.setPosition(20, 20);

        objects = new Array<>();
        bedside = new Bedside(30, 20);
        abyss = new Abyss(0, 0);
        abyss.toCenter();
        mirror = new Mirror(630, 290);
        picture = new Picture(40, 270);
        safe = new Safe(550, 180);

        close = false;

        fillArray();
    }

    private void fillArray() {
        objects.add(bedside);
        objects.add(abyss);
        objects.add(mirror);
        objects.add(picture);
        objects.add(safe);
    }

    public void update() {
        toClose();
        SoundLoader.backgroundMusic.stop();
        time = TimeUtils.nanoTime();
    }

    private void toClose() {
        close = true;
    }

    public boolean isClose() {
        return close;
    }

    Array<GameObject> getAllObjects() {
        return objects;
    }

    Sprite getBackground() {
        return sprite;
    }

    public void dispose() {
        for (GameObject object : objects) {
            object.dispose();
        }
    }
}
