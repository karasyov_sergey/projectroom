package ru.pitchblackteam.game.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.helpers.Arrow;
import ru.pitchblackteam.game.helpers.TextureLoader;
import ru.pitchblackteam.game.objects.GameObject;

public class ObjectRender {
    private Room game;
    private GameObject object;
    private SpriteBatch batch;
    private OrthographicCamera camera;
    private Arrow arrow;
    private Sprite sprite;
    private int tempX;
    private int tempY;

    public ObjectRender(Room game, GameObject object) {
        this.game = game;
        this.object = object;
        batch = new SpriteBatch();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, Room.WIDTH, Room.HEIGHT);

        sprite = new Sprite(TextureLoader.mainBackground);
        sprite.setSize(730, 480);

        Texture btnDownTexture = TextureLoader.buttonDown;
        arrow = new Arrow(btnDownTexture, (int) (Room.WIDTH / 2 - btnDownTexture.getWidth() / 2), 5);

        tempX = object.getX();
        tempY = object.getY();

        object.toCenter();
    }

    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        batch.setProjectionMatrix(camera.combined);

        batch.begin();
        sprite.draw(batch);
        if (object != null) {
            object.render(batch);
        }
        arrow.render(batch);
        game.inventory.render(batch);
        batch.end();

        if (arrow.isTouched(camera)) {
            restoreLocation();
            game.setScreen(game.mainSideScreen);
        }

        game.inventory.fixateTouch();
        object.update(game);
    }

    private void restoreLocation() {
        object.toMinSize();
        object.setX(tempX);
        object.setY(tempY);
    }
}
