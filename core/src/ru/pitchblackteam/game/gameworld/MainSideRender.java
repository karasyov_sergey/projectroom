package ru.pitchblackteam.game.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.helpers.TextureLoader;
import ru.pitchblackteam.game.objects.GameObject;

public class MainSideRender {
    private Room game;
    private OrthographicCamera camera;
    private SpriteBatch batch;
    private Array<GameObject> objects;
    private Sprite sprite;
    private MainSideWorld world;

    public MainSideRender(Room game, MainSideWorld mainSideWorld) {
        this.game = game;
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Room.WIDTH, Room.HEIGHT);
        world = mainSideWorld;
        sprite = mainSideWorld.getBackground();
        objects = mainSideWorld.getAllObjects();
    }

    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        batch.setProjectionMatrix(camera.combined);


        if (!world.isClose()) {
            batch.begin();
            sprite.draw(batch);
            for (GameObject object : objects) {
                object.render(batch);
            }
            game.inventory.render(batch);
            batch.end();

            for (GameObject object : objects) {
                if (object.justTouched()) {
                    object.toFullSize();
                    game.objectScreen.giveObject(object);
                    game.setScreen(game.objectScreen);
                }
            }

            game.inventory.fixateTouch();
        } else {
            batch.begin();
            batch.draw(TextureLoader.shadow, Room.WIDTH / 2 - TextureLoader.shadow.getWidth() / 2f, 0);
            batch.end();
        }

        if (world.time != 0 && TimeUtils.nanoTime() - world.time >= 5_000_000_000L) {
            game.setScreen(game.gameOverScreen);
        }
    }
}
