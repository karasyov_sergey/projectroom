package ru.pitchblackteam.game.enums;

public enum ItemType {
    LIGHT,
    TRUMPET,
    HAMMER,
    LIGHTER
}
