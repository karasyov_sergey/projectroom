package ru.pitchblackteam.game.game;

import com.badlogic.gdx.Game;

import ru.pitchblackteam.game.gameworld.Items;
import ru.pitchblackteam.game.helpers.Data;
import ru.pitchblackteam.game.helpers.Inventory;
import ru.pitchblackteam.game.helpers.SoundLoader;
import ru.pitchblackteam.game.helpers.TextureLoader;
import ru.pitchblackteam.game.screens.BoxScreen;
import ru.pitchblackteam.game.screens.GameOverScreen;
import ru.pitchblackteam.game.screens.MainSideScreen;
import ru.pitchblackteam.game.screens.ObjectScreen;
import ru.pitchblackteam.game.screens.PageScreen;

public class Room extends Game {
    public MainSideScreen mainSideScreen;
    public BoxScreen boxScreenOne;
    public BoxScreen boxScreenTwo;
    public BoxScreen boxScreenThree;
    public GameOverScreen gameOverScreen;
    public ObjectScreen objectScreen;
    public PageScreen pageScreen;
    public Inventory inventory;
    public static final float WIDTH = 800;
    public static final float HEIGHT = 480;

    @Override
    public void create() {
        initializeTheGame();
    }

    public void initializeTheGame() {
        TextureLoader.load();
        SoundLoader.load();
        Data.initialData();
        Items.createItems();

        inventory = new Inventory();

        mainSideScreen = new MainSideScreen(this);
        objectScreen = new ObjectScreen(this);
        boxScreenOne = new BoxScreen(this, Items.book, BoxScreen.BoxType.USUAL);
        boxScreenTwo = new BoxScreen(this, Items.itemBox, BoxScreen.BoxType.USUAL);
        boxScreenThree = new BoxScreen(this, Items.trumpet, BoxScreen.BoxType.UNUSUAL);
        pageScreen = new PageScreen(this);
        gameOverScreen = new GameOverScreen(this);
        setScreen(mainSideScreen);
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
        TextureLoader.dispose();
        SoundLoader.dispose();
    }
}
