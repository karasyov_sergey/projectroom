package ru.pitchblackteam.game.objects;

import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.helpers.TextureLoader;

public class SecretButton extends GameObject{
    private boolean touched;

    public SecretButton(int x, int y) {
        super(TextureLoader.buttonSecret, TextureLoader.buttonSecret, x, y);
        touched = false;
    }

    @Override
    public void update(Room game) {
        if (justTouched()){
            touched = true;
        }
    }

    public boolean isTouched() {
        return touched;
    }
}
