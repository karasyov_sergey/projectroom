package ru.pitchblackteam.game.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import ru.pitchblackteam.game.enums.ItemType;
import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.helpers.TextureLoader;

public class Sheet extends GameObject {
    private String content;
    private BitmapFont bitmapFont;
    private boolean isHeated;

    public Sheet(String content) {
        super(TextureLoader.smallSheet, TextureLoader.bigSheet, 0, 0);
        this.content = content;
        isHeated = false;
        bitmapFont = new BitmapFont(Gdx.files.internal("fonts/font.fnt"));
        bitmapFont.setColor(Color.RED);
        bitmapFont.getData().setScale(0.58f, 0.58f);
    }

    @Override
    public void update(Room game) {
        try {
            if (justTouched() && !game.inventory.getTouchedCell().isEmpty()
                    && game.inventory.getTouchedCell().
                    getCellContent().getType().equals(ItemType.LIGHTER)) {
                toHeat();
                game.inventory.deleteItem();
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {

        }

    }

    private void toHeat() {
        isHeated = true;
    }

    @Override
    public void render(SpriteBatch batch) {
        super.render(batch);
        if (isHeated && !getRenderTexture().equals(TextureLoader.smallSheet)) {
            bitmapFont.draw(batch, content, getX() + 15, getY() + getHeight() - 20);
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        bitmapFont.dispose();
    }
}
