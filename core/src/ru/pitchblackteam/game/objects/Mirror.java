package ru.pitchblackteam.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import ru.pitchblackteam.game.enums.ItemType;
import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.gameworld.Items;
import ru.pitchblackteam.game.helpers.SoundLoader;
import ru.pitchblackteam.game.helpers.TextureLoader;

/**
 * Класс для представления
 * объекта "Зеркало"
 *
 * @author Лисова Анастасия, 17ИТ17
 */
@SuppressWarnings("unused")
public class Mirror extends GameObject {
    private Container container;
    private boolean open;

    public Mirror(int x, int y) {
        super(TextureLoader.smallMirror, TextureLoader.bigMirror, x, y);
        container = new Container(Items.lighter, 640, 330);
        open = false;
    }

    @Override
    public void update(Room game) {
        try {
            if (justTouched() && !game.inventory.getTouchedCell().isEmpty()
                    && game.inventory.getTouchedCell().
                    getCellContent().getType().equals(ItemType.TRUMPET)) {
                SoundLoader.mirror.play();
                game.inventory.deleteItem();
                toOpen();
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {

        }
        if (open){
            container.update(game);
        }
    }

    private void toOpen() {
        open = true;
    }

    @Override
    public void setX(int x) {
        super.setX(x);
        container.setX(640);
    }

    @Override
    public void setY(int y) {
        super.setY(y);
        container.setY(330);
    }

    @Override
    public void toCenter() {
        super.toCenter();
        container.setX(getX() + 10);
        container.setY(getY() + getHeight() / 4);
    }

    @Override
    public void toFullSize() {
        super.toFullSize();
        container.toFullSize();
    }

    @Override
    public void toMinSize() {
        super.toMinSize();
        container.toMinSize();
    }

    @Override
    public void render(SpriteBatch batch) {
        container.render(batch);
        if (!open) {
            super.render(batch);
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        container.dispose();
    }
}
