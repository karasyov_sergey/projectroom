package ru.pitchblackteam.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import ru.pitchblackteam.game.enums.ItemType;
import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.gameworld.Items;
import ru.pitchblackteam.game.helpers.SoundLoader;
import ru.pitchblackteam.game.helpers.TextureLoader;


@SuppressWarnings("unused")
public class Picture extends GameObject {
    private Nail nail;
    private Rope rope;
    private Container container;
    private boolean isOpen;

    public Picture(int x, int y) {
        super(TextureLoader.smallPicture, TextureLoader.bigPicture, x, y);
        rope = new Rope(getX() + 1, getY() + getHeight());
        nail = new Nail((int) (getX() + rope.getX() + rope.getX() * 0.17f),
                getY() + getHeight() + rope.getHeight() / 2 + 5);
        container = new Container(Items.sheet, 55, 280);
        isOpen = false;
    }

    @Override
    public void update(Room game) {
        try {
            if (nail.justTouched() && !game.inventory.getTouchedCell().isEmpty()
                    && game.inventory.getTouchedCell().
                    getCellContent().getType().equals(ItemType.HAMMER)) {
                SoundLoader.hammer.play();
                toOpen();
                game.inventory.deleteItem();
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {

        }
        if (isOpen) {
            container.update(game);
        }
    }

    private void toOpen() {
        isOpen = true;
    }

    @Override
    public void toFullSize() {
        super.toFullSize();
        rope.toFullSize();
        nail.toFullSize();
        container.toFullSize();
    }

    @Override
    public void toMinSize() {
        super.toMinSize();
        rope.toMinSize();
        nail.toMinSize();
        container.toMinSize();
    }

    @Override
    public void toCenter() {
        setX(((int) (Room.WIDTH / 2 - getWidth() / 2)));
        setY(50);
        rope.setX(getX() + 1);
        rope.setY(getY() + getHeight());
        nail.setX((int) ((getX() + rope.getX() / 2) + rope.getX() * 0.1f));
        nail.setY(getY() + getHeight() + rope.getHeight() / 2);
        container.setX((int) (Room.WIDTH / 2 - getX() / 2));
        container.setY(60);
    }

    @Override
    public void setX(int x) {
        super.setX(x);
        rope.setX(getX() + 1);
        nail.setX((int) (getX() + rope.getX() + rope.getX() * 0.17f));
        container.setX(55);
    }

    @Override
    public void setY(int y) {
        super.setY(y);
        rope.setY(getY() + getHeight());
        nail.setY(getY() + getHeight() + rope.getHeight() / 2 + 5);
        container.setY(280);
    }

    @Override
    public void render(SpriteBatch batch) {
        container.render(batch);
        if (!isOpen) {
            super.render(batch);
            rope.render(batch);
        }
        nail.render(batch);
    }

    @Override
    public void dispose() {
        super.dispose();
        nail.dispose();
        rope.dispose();
        container.dispose();
    }

    private class Nail extends GameObject {

        Nail(int x, int y) {
            super(TextureLoader.smallNail, TextureLoader.bigNail, x, y);
        }

        @Override
        public void update(Room game) {

        }
    }

    private class Rope extends GameObject {

        Rope(int x, int y) {
            super(TextureLoader.smallRope, TextureLoader.bigRope, x, y);
        }

        @Override
        public void update(Room game) {

        }
    }

}
