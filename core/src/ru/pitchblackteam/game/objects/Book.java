package ru.pitchblackteam.game.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.helpers.Data;
import ru.pitchblackteam.game.helpers.SoundLoader;
import ru.pitchblackteam.game.helpers.TextureLoader;

public class Book extends GameObject {

    private Array<Page> pages;
    private String numbers;
    private Array<String> data;

    public Book(int x, int y) {
        super(TextureLoader.book, TextureLoader.book, x, y);
        numbers = "68374";
        data = Data.bookOneData;
        createPages();
    }

    private void createPages() {
        pages = new Array<>();
        for (int i = 0; i < 5; i++) {
            pages.add(new Page(0, 0, data.get(i)
                    , String.valueOf(numbers.charAt(i))));
        }
    }

    @Override
    public void update(Room game) {
        if (game.pageScreen.getPages() == null) {
            game.pageScreen.givePages(pages);
        }
        if (justTouched()) {
            SoundLoader.bookOpen.play();
            game.setScreen(game.pageScreen);
        }
    }


    public class Page extends GameObject {

        private BitmapFont pageNumber;
        private BitmapFont text;
        private String number;
        private String data;

        Page(int x, int y, String data, String number) {
            super(TextureLoader.bookPage, TextureLoader.bookPage, x, y);

            this.number = number;
            this.data = data;
            pageNumber = new BitmapFont(Gdx.files.internal("fonts/font.fnt"));
            pageNumber.setColor(Color.BLACK);
            text = new BitmapFont(Gdx.files.internal("fonts/font.fnt"));
            text.setColor(Color.BLACK);
            text.getData().setScale(0.55f, 0.55f);

        }

        @Override
        public void render(SpriteBatch batch) {
            super.render(batch);
            pageNumber.draw(batch, number, getRenderTexture().getWidth() / 4, 50);
            text.draw(batch, data, 60, Room.HEIGHT - 55);
        }


        @Override
        public void update(Room game) {

        }
    }
}
