package ru.pitchblackteam.game.objects;

import com.badlogic.gdx.graphics.Texture;

import ru.pitchblackteam.game.enums.ItemType;
import ru.pitchblackteam.game.game.Room;

public class GameItem extends GameObject {
    private ItemType type;
    private boolean pickedUp;

    public GameItem(Texture smallTexture, Texture bigTexture, ItemType type, int x, int y) {
        super(smallTexture, bigTexture, x, y);
        this.type = type;
        pickedUp = false;
    }

    ItemType getType() {
        return type;
    }

    @Override
    public void update(Room game) {

    }

    public void toPickedUp() {
        this.pickedUp = true;
    }

    boolean isNotPickedUp() {
        return !pickedUp;
    }
}
