package ru.pitchblackteam.game.objects;


import ru.pitchblackteam.game.enums.ItemType;
import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.helpers.SoundLoader;
import ru.pitchblackteam.game.helpers.TextureLoader;

/**
 * Класс для представления
 * объекта "Дверь"
 *
 * @author Лисова Анастасия, 17ИТ17
 */
@SuppressWarnings("unused")
public class Abyss extends GameObject {
    private boolean isOpen;
    private double counter = 0.1;

    public Abyss(int x, int y) {
        super(TextureLoader.door, TextureLoader.abyss, x, y);

        isOpen = false;
    }

    @Override
    public void update(Room game) {
        try {
            if (justTouched() && !game.inventory.getTouchedCell().isEmpty()
                    && game.inventory.getTouchedCell().
                    getCellContent().getType().equals(ItemType.LIGHT)) {
                toOpen();
                game.inventory.deleteItem();
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {

        }

        if (isOpen() && justTouched()) {
            game.setScreen(game.gameOverScreen);
        }

        if (!isOpen() && justTouched()) {
            counter += 0.3;
            if (counter > 1) {
                game.mainSideScreen.getWorld().update();
                game.setScreen(game.mainSideScreen);
            } else {
                SoundLoader.growl.setVolume(SoundLoader.growl.play(), (float) counter);
            }
        }
    }


    private boolean isOpen() {
        return isOpen;
    }

    private void toOpen() {
        isOpen = true;
    }

}

