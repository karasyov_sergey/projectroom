package ru.pitchblackteam.game.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.helpers.TextureLoader;

public class Container extends GameObject {
    private GameItem content;
    private Sheet sheet;

    Container(GameItem content, int x, int y) {
        super(TextureLoader.smallContainer, TextureLoader.bigContainer, x, y);
        this.content = content;
        this.content.setX(x + getWidth() / 2 - content.getWidth() / 2);
        this.content.setY(y + getHeight() / 2 - content.getHeight() / 2);
    }

    Container(Sheet sheet, int x, int y) {
        super(TextureLoader.smallContainer, TextureLoader.bigContainer, x, y);
        this.sheet = sheet;
        this.sheet.setX(x + getWidth() / 2 - sheet.getWidth() / 2);
        this.sheet.setY(y + getHeight() / 2 - sheet.getHeight() / 2);
    }

    @Override
    public void update(Room game) {
        if (content != null) {
            updateContent(game);
        }
        if (sheet != null) {
            sheet.update(game);
        }
    }

    private void updateContent(Room game) {
        if (content.justTouched() && content.isNotPickedUp()) {
            game.inventory.addItem(content);
        }
        if (game.inventory.haveGot(content)) {
            content.toPickedUp();
            content.toMinSize();
        }
    }

    @Override
    public void toFullSize() {
        super.toFullSize();
        if (content != null && content.isNotPickedUp()) {
            content.toFullSize();
        }
        if (sheet != null) {
            sheet.toFullSize();
        }
    }

    @Override
    public void setX(int x) {
        super.setX(x);
        if (sheet != null) {
            sheet.setX(getX() + getWidth() / 2 - sheet.getWidth() / 2);
        }
        if (content != null) {
            if (content.isNotPickedUp()) {
                content.setX(getX() + getWidth() / 2 - content.getWidth() / 2);
            }
        }
    }

    @Override
    public void setY(int y) {
        super.setY(y);
        if (sheet != null) {
            sheet.setY(getY() + getHeight() / 2 - sheet.getHeight() / 2);
        }
        if (content != null) {
            if (content.isNotPickedUp()) {
                content.setY(getY() + getHeight() / 2 - content.getHeight() / 2);
            }
        }
    }

    @Override
    public void toMinSize() {
        super.toMinSize();
        if (content != null && content.isNotPickedUp()) {
            content.toMinSize();
        }
        if (sheet != null) {
            sheet.toMinSize();
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        super.render(batch);
        if (content != null) {
            renderContent(batch);
        }
        if (sheet != null) {
            renderSheet(batch);
        }
    }

    private void renderSheet(SpriteBatch batch) {
        sheet.render(batch);
    }

    private void renderContent(SpriteBatch batch) {
        if (content.isNotPickedUp()) {
            content.render(batch);
        }
    }

    @Override
    public void dispose() {
        if (content != null) {
            content.dispose();
        }
        if (sheet != null) {
            sheet.dispose();
        }
    }
}
