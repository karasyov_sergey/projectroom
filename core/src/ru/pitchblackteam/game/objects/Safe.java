package ru.pitchblackteam.game.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.gameworld.Items;
import ru.pitchblackteam.game.helpers.SoundLoader;
import ru.pitchblackteam.game.helpers.TextureLoader;

/**
 * Класс для представления
 * объекта "Сейф"
 *
 * @author Лисова Анастасия, 17ИТ17
 */
@SuppressWarnings("unused")
public class Safe extends GameObject {
    private Array<Button> buttons;
    private BitmapFont font;
    private String numbers;
    private String password;
    private int counter;
    private boolean isOpen;
    private Container container;


    public Safe(int x, int y) {
        super(TextureLoader.smallSafe, TextureLoader.safe, x, y);
        font = new BitmapFont(Gdx.files.internal("fonts/font.fnt"));
        font.setColor(Color.RED);
        font.getData().setScale(0.4f,0.4f);
        numbers = "-----";
        password = "87643";
        counter = 0;
        isOpen = false;
        createButtons();
        container = new Container(Items.light, 570, 195);
    }

    private void createButtons() {
        buttons = new Array<>();
        int tempX = (int) (getX() + getWidth() / 2.5);
        int tempY = (int) (getY() + getHeight() / 1.5);
        for (int i = 1; i <= 9; i++) {
            buttons.add(new Button(TextureLoader.safeButtonSmall,
                    TextureLoader.safeButtonBig, i, tempX, tempY));
            tempX += buttons.get(i - 1).getWidth();
            if (i % 3 == 0) {
                tempY -= buttons.get(i - 1).getHeight();
                tempX -= buttons.get(i - 1).getWidth() * 3;
            }
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        container.render(batch);
        if (!isOpen()) {
            super.render(batch);
            for (Button button : buttons) {
                button.render(batch);
            }
            font.draw(batch, numbers, getX() + getWidth() * 0.1f, (int) (getY() + getHeight() / 1.5));
        }
    }


    public void dispose() {
        super.dispose();
        font.dispose();
        for (Button button : buttons) {
            button.dispose();
        }
    }

    @Override
    public void update(Room game) {
        for (Button button : buttons) {
            if (button.justTouched() && !isOpen()) {
                SoundLoader.button.play();
                numbers = numbers.replaceFirst("-", String.valueOf(button.getNumber()));
                counter++;
            }
            if (!numbers.equals(password) && counter == 5) {
                numbers = "-----";
                game.mainSideScreen.getWorld().update();
                game.setScreen(game.mainSideScreen);
            }
            if (numbers.equals(password)) {
                isOpen = true;
            }
        }
        if (isOpen) {
            container.update(game);
        }
    }

    private boolean isOpen() {
        return isOpen;
    }

    @Override
    public void toCenter() {
        setX((int) (Room.WIDTH / 2 - getWidth() / 2));
        setY((int) (Room.HEIGHT / 2 - getHeight() / 2));
        container.setX((int) (Room.WIDTH / 2 - getX()));
        container.setY(130);
    }

    @Override
    public void setX(int x) {
        super.setX(x);
        int tempX = (int) (getX() + getWidth() / 2.5);
        for (int i = 1; i <= buttons.size; i++) {
            buttons.get(i - 1).setX(tempX);
            tempX += buttons.get(i - 1).getWidth();
            if (i % 3 == 0) {
                tempX -= buttons.get(i - 1).getWidth() * 3;
            }
        }
        container.setX(getX() + 20);
    }

    @Override
    public void setY(int y) {
        super.setY(y);
        int tempY = (int) (getY() + getHeight() / 1.5);
        for (int i = 1; i <= buttons.size; i++) {
            buttons.get(i - 1).setY(tempY);
            if (i % 3 == 0) {
                tempY -= buttons.get(i - 1).getHeight();
            }
        }
        container.setY(getY() + 10);
    }

    @Override
    public void toFullSize() {
        super.toFullSize();
        for (Button button : buttons) {
            button.toFullSize();
        }
        font.getData().setScale(2f, 2f);
        container.toFullSize();
    }

    @Override
    public void toMinSize() {
        super.toMinSize();
        for (Button button : buttons) {
            button.toMinSize();
        }
        font.getData().setScale(0.4f, 0.4f);
        container.toMinSize();
    }
}

