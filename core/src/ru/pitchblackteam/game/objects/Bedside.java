package ru.pitchblackteam.game.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.helpers.SoundLoader;
import ru.pitchblackteam.game.helpers.TextureLoader;


public class Bedside extends GameObject {

    private Box boxOne;
    private Box boxTwo;
    private Box boxThree;
    private Box boxFour;


    public Bedside(int x, int y) {
        super(x, y);

        boxFour = new Box(TextureLoader.smallBoxFour, TextureLoader.bigBoxFour, x, y);

        boxThree = new Box(TextureLoader.smallBoxThree, TextureLoader.bigBoxThree, x,
                boxFour.getHeight() + y);

        boxTwo = new Box(TextureLoader.smallBoxTwo, TextureLoader.bigBoxTwo, x,
                boxFour.getHeight() +
                        boxThree.getHeight()
                        + y);

        boxOne = new Box(TextureLoader.smallBoxOne, TextureLoader.bigBoxOne, x,
                boxFour.getHeight() +
                        boxThree.getHeight()
                        + boxTwo.getHeight() + y);

        setWidth(boxOne.getWidth());
        setHeight(boxOne.getHeight() + boxTwo.getHeight()
                + boxThree.getHeight() + boxFour.getHeight());

        toMinSize();


    }

    @Override
    public void render(SpriteBatch batch) {
        boxOne.render(batch);
        boxTwo.render(batch);
        boxThree.render(batch);
        boxFour.render(batch);
    }

    public void dispose() {
        boxOne.dispose();
        boxTwo.dispose();
        boxThree.dispose();
        boxFour.dispose();

    }

    @Override
    public void update(Room game) {
        if (boxOne.justTouched()) {
            SoundLoader.bedsideOpen.play();
            game.setScreen(game.boxScreenOne);
        }
        if (boxTwo.justTouched()) {
            SoundLoader.bedsideOpen.play();
            game.setScreen(game.boxScreenTwo);
        }
        if (boxThree.justTouched()) {
            SoundLoader.bedsideOpen.play();
            game.setScreen(game.boxScreenThree);
        }
    }

    @Override
    public void toCenter() {
        boxFour.setX((int) (Room.WIDTH / 2 - boxFour.getWidth() / 2));
        boxFour.setY((int) (boxFour.getY() * 2.5));
        boxThree.setX((int) (Room.WIDTH / 2 - boxThree.getWidth() / 2));
        boxThree.setY(boxFour.getY() + boxFour.getHeight());
        boxTwo.setX((int) (Room.WIDTH / 2 - boxTwo.getWidth() / 2));
        boxTwo.setY(boxThree.getY() + boxThree.getHeight());
        boxOne.setX((int) (Room.WIDTH / 2 - boxOne.getWidth() / 2));
        boxOne.setY(boxTwo.getY() + boxTwo.getHeight());
    }

    @Override
    public boolean justTouched() {
        return boxOne.justTouched() ||
                boxTwo.justTouched() ||
                boxThree.justTouched() ||
                boxFour.justTouched();
    }

    @Override
    public void setX(int x) {
        boxFour.setX(x);
        boxThree.setX(x);
        boxTwo.setX(x);
        boxOne.setX(x);
    }

    @Override
    public void setY(int y) {
        boxFour.setY(y);
        boxThree.setY(boxFour.getHeight() + y);
        boxTwo.setY(boxFour.getHeight() + boxThree.getHeight() + y);
        boxOne.setY(boxFour.getHeight() + boxThree.getHeight() + boxTwo.getHeight() + y);
    }

    @Override
    public void toFullSize() {
        boxOne.toFullSize();
        boxTwo.toFullSize();
        boxThree.toFullSize();
        boxFour.toFullSize();
    }

    @Override
    public void toMinSize() {
        boxOne.toMinSize();
        boxTwo.toMinSize();
        boxThree.toMinSize();
        boxFour.toMinSize();

    }

    private class Box extends GameObject {

        Box(Texture smallTexture, Texture bigTexture, int x, int y) {
            super(smallTexture, bigTexture, x, y);
            toMinSize();
        }

        @Override
        public void update(Room game) {

        }
    }

}
