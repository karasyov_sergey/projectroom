package ru.pitchblackteam.game.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import ru.pitchblackteam.game.game.Room;

public class Button extends GameObject {
    private BitmapFont font;
    private int number;

    Button(Texture smallTexture, Texture bigTexture, int number, int x, int y) {
        super(smallTexture, bigTexture, x, y);
        this.number = number;
        font = new BitmapFont(Gdx.files.internal("fonts/font.fnt"));
        font.setColor(Color.RED);
        toMinSize();
    }

    int getNumber() {
        return number;
    }


    @Override
    public void render(SpriteBatch batch) {
        super.render(batch);
        font.draw(batch, String.valueOf(number),
                getX() + getWidth() / 2.5f, getY() + getHeight() / 1.5f);
    }

    @Override
    public void dispose() {
        super.dispose();
        font.dispose();
    }

    @Override
    public void update(Room game) {

    }

    @Override
    public void toFullSize() {
        super.toFullSize();
        font.getData().setScale(1.5f, 1.5f);
    }

    @Override
    public void toMinSize() {
        super.toMinSize();
        font.getData().setScale(0.5f, 0.5f);
    }
}
