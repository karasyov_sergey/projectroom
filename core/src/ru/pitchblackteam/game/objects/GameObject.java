package ru.pitchblackteam.game.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import ru.pitchblackteam.game.game.Room;


@SuppressWarnings("unused")
public abstract class GameObject {
    private Rectangle rectangle;
    private Vector3 touchPos;
    private OrthographicCamera camera;
    private Texture renderTexture;
    private Texture smallTexture;
    private Texture bigTexture;
    private boolean overlapsByX;
    private boolean overlapsByY;

    GameObject(Texture smallTexture, Texture bigTexture, int x, int y) {
        this.smallTexture = smallTexture;
        this.bigTexture = bigTexture;
        renderTexture = smallTexture;

        rectangle = new Rectangle();
        initializeSize();
        rectangle.x = x;
        rectangle.y = y;

        touchPos = new Vector3();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Room.WIDTH, Room.HEIGHT);

        overlapsByX = false;
        overlapsByY = false;

    }

    GameObject(int x, int y) {
        rectangle = new Rectangle();
        rectangle.x = x;
        rectangle.y = y;
        rectangle.width = 0;
        rectangle.height = 0;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Room.WIDTH, Room.HEIGHT);

        overlapsByX = false;
        overlapsByY = false;
    }

    public boolean justTouched() {
        if (Gdx.input.justTouched()) {
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);
            overlapsByX = touchPos.x >= rectangle.x &&
                    touchPos.x <= rectangle.x + rectangle.width;
            overlapsByY = touchPos.y >= rectangle.y &&
                    touchPos.y <= rectangle.y + rectangle.height;
            return overlapsByX && overlapsByY;
        }
        return false;
    }

    public void toFullSize() {
        renderTexture = bigTexture;
        initializeSize();
    }


    public void toMinSize() {
        renderTexture = smallTexture;
        initializeSize();
    }


    public void render(SpriteBatch batch) {
        if (renderTexture != null) {
            batch.draw(renderTexture, rectangle.x, rectangle.y);
        }
    }

    public void dispose() {
        renderTexture.dispose();
        smallTexture.dispose();
        bigTexture.dispose();
    }

    public abstract void update(Room game);

    public void toCenter() {
        rectangle.x = ((int) (Room.WIDTH / 2 - rectangle.width / 2));
        rectangle.y = ((int) (Room.HEIGHT / 2 - rectangle.height / 2));
    }

    private void initializeSize() {
        rectangle.width = renderTexture.getWidth();
        rectangle.height = renderTexture.getHeight();
    }

    public int getX() {
        return (int) rectangle.getX();
    }

    public int getY() {
        return (int) rectangle.getY();
    }

    public void setX(int x) {
        rectangle.x = x;
    }

    public void setY(int y) {
        rectangle.y = y;
    }

    public void setWidth(int width) {
        rectangle.width = width;
    }

    public void setHeight(int height) {
        rectangle.height = height;
    }

    public int getWidth() {
        return (int) rectangle.getWidth();
    }

    public int getHeight() {
        return (int) rectangle.getHeight();
    }

    Texture getRenderTexture() {
        return renderTexture;
    }
}
