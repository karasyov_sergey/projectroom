package ru.pitchblackteam.game.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.gameworld.Items;
import ru.pitchblackteam.game.helpers.SoundLoader;
import ru.pitchblackteam.game.helpers.TextureLoader;

public class ItemBox extends GameObject {
    private Carcass carcass;
    private Cap cap;

    public ItemBox(int x, int y) {
        super(x, y);
        carcass = new Carcass(TextureLoader.itemBox, TextureLoader.itemBox, x, y);
        cap = new Cap(TextureLoader.cap, TextureLoader.cap, x, y);
    }

    @Override
    public void update(Room game) {
        cap.update(game);
        if (cap.isOpen() && carcass.getContent() != null) {
            carcass.update(game);
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        carcass.render(batch);
        if (!cap.isOpen()) {
            cap.render(batch);
        }

    }

    @Override
    public void dispose() {
        super.dispose();
        carcass.dispose();
        cap.dispose();
    }

    class Carcass extends GameObject {
        private GameItem content;

        Carcass(Texture smallTexture, Texture bigTexture, int x, int y) {
            super(smallTexture, bigTexture, x, y);
            content = Items.hammer;
            content.toFullSize();
            this.content.setX(x + getWidth() / 2 - content.getWidth() / 2);
            this.content.setY(y + getHeight() / 2 - content.getHeight() / 2);
        }

        @Override
        public void update(Room game) {
            if (content.justTouched()) {
                game.inventory.addItem(content);

            }
            if (game.inventory.haveGot(content)) {
                content.toPickedUp();
                content.toMinSize();
                content = null;
            }
        }

        @Override
        public void render(SpriteBatch batch) {
            super.render(batch);
            if (content != null) {
                content.render(batch);
            }
        }

        GameItem getContent() {
            return content;
        }
    }

    class Cap extends GameObject {
        private boolean isOpen;
        private Array<Button> buttons;
        private BitmapFont code;
        private BitmapFont inputCode;
        private String currentCode;
        private String password;
        private int counter;

        Cap(Texture smallTexture, Texture bigTexture, int x, int y) {
            super(smallTexture, bigTexture, x, y);
            createButtons();
            toFullButtonSize();
            inputCode = new BitmapFont(Gdx.files.internal("fonts/font.fnt"));
            inputCode.setColor(Color.RED);
            inputCode.getData().setScale(2f, 2f);
            code = new BitmapFont(Gdx.files.internal("fonts/font.fnt"));
            code.setColor(Color.RED);
            code.getData().setScale(0.6f,0.6f);
            currentCode = "----";
            password = "6437";
            counter = 0;
            isOpen = false;
        }

        private void toFullButtonSize() {
            for (Button button : buttons) {
                button.toFullSize();
            }
        }

        @Override
        public void update(Room game) {
            for (Button button : buttons) {
                if (button.justTouched() && !isOpen()) {
                    SoundLoader.button.play();
                    currentCode = currentCode.replaceFirst("-", String.valueOf(button.getNumber()));
                    counter++;
                }
                if (!currentCode.equals(password) && counter == 4) {
                    currentCode = "----";
                    counter = 0;
                }
                if (currentCode.equals(password)) {
                    isOpen = true;
                }
            }

        }

        boolean isOpen() {
            return isOpen;
        }

        private void createButtons() {
            buttons = new Array<>();
            int tempX = (int) (getX() + getWidth() / 2.5);
            int tempY = (int) (getY() + getHeight() / 1.5);
            for (int i = 1; i <= 9; i++) {
                buttons.add(new Button(TextureLoader.safeButtonBig,
                        TextureLoader.safeButtonBig, i, tempX, tempY));
                tempX += buttons.get(i - 1).getWidth();
                if (i % 3 == 0) {
                    tempY -= buttons.get(i - 1).getHeight();
                    tempX -= buttons.get(i - 1).getWidth() * 3;
                }
            }
        }


        @Override
        public void render(SpriteBatch batch) {
            super.render(batch);
            for (int i = 0; i < buttons.size; i++) {
                buttons.get(i).render(batch);
            }
            inputCode.draw(batch, currentCode, getX() + getX() * 0.1f, getY() * 2.5f);
            code.draw(batch, "1\n1\n0\n0\n1\n0\n0\n1\n0\n0\n1\n0\n1",
                    getX() + getWidth() - 15, getY() + getHeight() - 10);
        }

        @Override
        public void dispose() {
            super.dispose();
            for (int i = 0; i < buttons.size; i++) {
                buttons.get(i).dispose();
            }
            code.dispose();
            inputCode.dispose();
        }
    }
}
