package ru.pitchblackteam.game.animation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.GdxRuntimeException;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;

public class SmoothTransition {
    private Stage stage;

    public SmoothTransition() {

    }

    public void initAnimation() {
        stage = new Stage();

        Image topLayer = new Image(getTexture());
        topLayer.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        topLayer.addAction(fadeOut(0.5f));
        stage.addActor(topLayer);
    }

    public static Texture getTexture() {
        Pixmap pixmap;
        try {
            pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        } catch (GdxRuntimeException e) {
            pixmap = new Pixmap(1, 1, Pixmap.Format.RGB565);
        }
        pixmap.setColor(Color.BLACK);
        pixmap.drawRectangle(0, 0, 1, 1);
        return new Texture(pixmap);
    }

    public void render() {
        stage.act();
        stage.draw();
    }

    public void dispose() {
        stage.dispose();
    }
}
