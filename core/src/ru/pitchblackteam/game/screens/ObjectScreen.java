package ru.pitchblackteam.game.screens;

import com.badlogic.gdx.Screen;

import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.gameworld.Items;
import ru.pitchblackteam.game.gameworld.ObjectRender;
import ru.pitchblackteam.game.objects.GameObject;

public class ObjectScreen implements Screen {
    private Room game;
    private ObjectRender objectRender;

    public ObjectScreen(Room game) {
        this.game = game;
    }

    public void giveObject(GameObject object) {
        objectRender = new ObjectRender(game, object);
    }

    @Override
    public void show() {
        Items.smoothTransition.initAnimation();
    }

    @Override
    public void render(float delta) {
        objectRender.render();
        Items.smoothTransition.render();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        Items.smoothTransition.dispose();
    }
}
