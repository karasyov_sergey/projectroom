package ru.pitchblackteam.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.gameworld.Items;
import ru.pitchblackteam.game.helpers.Arrow;
import ru.pitchblackteam.game.helpers.TextureLoader;
import ru.pitchblackteam.game.objects.GameItem;
import ru.pitchblackteam.game.objects.GameObject;
import ru.pitchblackteam.game.objects.SecretButton;

public class BoxScreen implements Screen {
    private Room game;
    private GameObject object;
    private Arrow arrow;
    private OrthographicCamera camera;
    private Sprite background;
    private SpriteBatch batch;
    private BoxType type;
    private GameItem content;
    private SecretButton secretButton;

    public BoxScreen(Room game, GameObject object, BoxType type) {
        this.type = type;
        switch (type) {
            case USUAL:
                initialize(game, object);
                break;
            case UNUSUAL:
                initialize(game, (GameItem) object);
                break;

        }
    }

    private void initialize(Room game, GameObject object) {
        this.game = game;
        this.object = object;
        generalInitialize();

    }

    private void initialize(Room game, GameItem item) {
        this.game = game;
        content = item;
        content.toFullSize();
        secretButton = new SecretButton(700, 400);
        generalInitialize();
    }

    private void generalInitialize() {
        arrow = new Arrow(TextureLoader.buttonDown,
                (int) (Room.WIDTH / 2 -
                        TextureLoader.buttonDown.getWidth() / 2),
                5);

        background = new Sprite(TextureLoader.boxBackground);

        batch = new SpriteBatch();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, Room.WIDTH, Room.HEIGHT);
    }

    @Override

    public void show() {
        Items.smoothTransition.initAnimation();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        batch.setProjectionMatrix(camera.combined);
        switch (type) {
            case USUAL:
                renderUsualBox();
                break;
            case UNUSUAL:
                renderSecretBox();
                break;
        }
        if (arrow.isTouched(camera)) {
            game.setScreen(game.objectScreen);
        }
        Items.smoothTransition.render();
    }

    private void renderSecretBox() {
        batch.begin();
        background.draw(batch);
        secretButton.render(batch);
        if (secretButton.isTouched() && content != null) {
            content.render(batch);
        }
        arrow.render(batch);
        batch.end();

        secretButton.update(game);
        if (content != null) {
            updateContent(game);
        }
    }

    private void renderUsualBox() {
        batch.begin();
        background.draw(batch);

        object.render(batch);

        arrow.render(batch);
        batch.end();

        object.update(game);
    }

    private void updateContent(Room game) {
        if (content.justTouched() && secretButton.isTouched()) {
            game.inventory.addItem(content);
        }
        if (game.inventory.haveGot(content)) {
            content.toPickedUp();
            content.toMinSize();
            content = null;
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        arrow.dispose();
        Items.smoothTransition.dispose();
    }

    public enum BoxType {
        USUAL,
        UNUSUAL
    }
}
