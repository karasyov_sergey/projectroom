package ru.pitchblackteam.game.screens;

import com.badlogic.gdx.Screen;

import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.gameworld.MainSideRender;
import ru.pitchblackteam.game.gameworld.MainSideWorld;

public class MainSideScreen implements Screen {
    private Room game;
    private MainSideWorld world;
    private MainSideRender render;

    public MainSideScreen(Room game) {
        this.game = game;
        world = new MainSideWorld(game);
        render = new MainSideRender(game, world);
    }

    @Override
    public void show() {
//        Items.smoothTransition.initAnimation();
//        if (!world.isClose()) {
//            SoundLoader.backgroundMusic.setVolume(0.25f);
//            SoundLoader.backgroundMusic.setLooping(true);
//            SoundLoader.backgroundMusic.play();
//        }
    }

    @Override
    public void render(float delta) {
        render.render();

//        Items.smoothTransition.render();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        world.dispose();
//        Items.smoothTransition.dispose();
    }

    public MainSideWorld getWorld() {
        return world;
    }
}
