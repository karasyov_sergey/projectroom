package ru.pitchblackteam.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.gameworld.Items;
import ru.pitchblackteam.game.helpers.Cell;
import ru.pitchblackteam.game.helpers.SoundLoader;

public class GameOverScreen implements Screen {
    private Room game;
    private OrthographicCamera camera;
    private SpriteBatch batch;
    private BitmapFont font;

    public GameOverScreen(Room game) {
        this.game = game;
        batch = new SpriteBatch();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, Room.WIDTH, Room.HEIGHT);

        font = new BitmapFont();
        font.setColor(Color.RED);
    }

    @Override
    public void show() {
        Items.smoothTransition.initAnimation();
        SoundLoader.backgroundMusic.stop();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        batch.setProjectionMatrix(camera.combined);

        batch.begin();
        font.draw(batch, "Game Over", Room.WIDTH / 2 - 35, Room.HEIGHT / 2);
        font.draw(batch, "Tap anywhere to start a new game", 100, 100);
        batch.end();

        if (Gdx.input.justTouched()) {
            Cell.clear();
            game.initializeTheGame();
        }

        Items.smoothTransition.render();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        font.dispose();
        Items.smoothTransition.dispose();
    }
}
