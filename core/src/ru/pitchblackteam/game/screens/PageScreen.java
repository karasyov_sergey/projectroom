package ru.pitchblackteam.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

import ru.pitchblackteam.game.game.Room;
import ru.pitchblackteam.game.gameworld.Items;
import ru.pitchblackteam.game.helpers.Arrow;
import ru.pitchblackteam.game.helpers.SoundLoader;
import ru.pitchblackteam.game.helpers.TextureLoader;
import ru.pitchblackteam.game.objects.Book;

public class PageScreen implements Screen {
    private Room game;
    private OrthographicCamera camera;
    private Array<Book.Page> pages;
    private SpriteBatch batch;
    private int index;
    private Arrow leftArrow;
    private Arrow rightArrow;
    private Arrow toBackArrow;


    public PageScreen(Room game) {
        this.game = game;
        leftArrow = new Arrow(TextureLoader.buttonLeft, 5, (int) (Room.HEIGHT / 2));
        rightArrow = new Arrow(TextureLoader.buttonRight, (int) (Room.WIDTH - 35),
                (int) (Room.HEIGHT / 2));
        toBackArrow = new Arrow(TextureLoader.buttonDown, (int) (Room.WIDTH / 2) -
                TextureLoader.buttonDown.getWidth() / 2, 0);
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Room.WIDTH, Room.HEIGHT);
        index = 0;
    }

    public void givePages(Array<Book.Page> pages) {
        this.pages = pages;
    }

    public Array<Book.Page> getPages() {
        return pages;
    }

    @Override
    public void show() {
        Items.smoothTransition.initAnimation();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        pages.get(index).render(batch);
        leftArrow.render(batch);
        rightArrow.render(batch);
        toBackArrow.render(batch);
        batch.end();

        if (leftArrow.isTouched(camera) && index > 0) {
            SoundLoader.leaf.play();
            show();
            index--;
            Items.smoothTransition.render();
        }

        if (rightArrow.isTouched(camera) && index != pages.size - 1) {
            SoundLoader.leaf.play();
            index++;
            show();
            Items.smoothTransition.render();
        }

        if (toBackArrow.isTouched(camera)) {
            show();
            game.setScreen(game.boxScreenOne);
        }

        Items.smoothTransition.render();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        Items.smoothTransition.dispose();
        toBackArrow.dispose();
        rightArrow.dispose();
        leftArrow.dispose();
        for (int i = 0; i < pages.size; i++) {
            pages.get(i).dispose();
        }
    }
}
